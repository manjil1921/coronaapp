package com.yantrashala.coronanepal.Model;

public class HomeModels {


    String id, title, source, date_added, long_description, short_description, image;

    public HomeModels(String id, String title, String source, String date_added, String long_description, String short_description, String image) {
        this.id = id;
        this.title = title;
        this.source = source;
        this.date_added = date_added;
        this.long_description = long_description;
        this.short_description = short_description;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

