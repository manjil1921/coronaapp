package com.yantrashala.coronanepal.Model;

public class EmergencyModel {
    String id,location, phone_number, contact_person_or_institute;

    public EmergencyModel(String id, String location, String phone_number, String contact_person_or_institute) {
        this.id = id;
        this.location = location;
        this.phone_number = phone_number;
        this.contact_person_or_institute = contact_person_or_institute;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getContact_person_or_institute() {
        return contact_person_or_institute;
    }

    public void setContact_person_or_institute(String contact_person_or_institute) {
        this.contact_person_or_institute = contact_person_or_institute;
    }
}
