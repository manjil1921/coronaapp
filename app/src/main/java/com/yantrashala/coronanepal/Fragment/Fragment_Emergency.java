package com.yantrashala.coronanepal.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.yantrashala.coronanepal.API.CoranaAPI;
import com.yantrashala.coronanepal.API.RetrofitHelper;
import com.yantrashala.coronanepal.Adapter.EmergencyServiceAdapter;
import com.yantrashala.coronanepal.Interface.PhoneListener;
import com.yantrashala.coronanepal.Model.EmergencyModel;
import com.yantrashala.coronanepal.R;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_Emergency extends Fragment {

    private static final int REQUEST_PHONE_CALL = 1;
    private ArrayList<EmergencyModel> emergencyServiceModels;
    private RecyclerView recyclerView;
    String id, locations, title,number;
    PhoneListener phoneListener;

    private View view;
    private EmergencyServiceAdapter emergencyServiceAdapter;

    SwipeRefreshLayout mswiperefresh;
    ProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_emergency, container, false);
        mswiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.mswiprefresh);
        progress=new ProgressDialog(getContext());
        refreshData();
        return view;
    }

    public void refreshData()
    {
        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);

                if(isconnectd()){
                    getdata();
                    Toast.makeText(getContext(),"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

                else{

                    Toast.makeText(getContext(),"No internet Connection",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });

    }


    public boolean isconnectd() {
        ConnectivityManager mconn = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo = mconn.getActiveNetworkInfo();
        if (minfo != null && minfo.isConnected()) {
            return true;
        }
        return false;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findview();
    }
    public void findview() {

        recyclerView = getView().findViewById(R.id.recyclerviewemergencyservice);
        init();
    }

    public void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        getdata();
        onAdapterListeners();
    }
    private void getdata() {
        progress.setMessage("Loading profile...");
        progress.show();
        CoranaAPI service = (CoranaAPI) RetrofitHelper.getInstance().getService(CoranaAPI.class);
        Call<JsonElement> call = service.getImpNumber();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()){
                    String responseValue = response.body().toString();
                    JSONArray notification = null;
                    try {

                        JSONObject jsonObject = new JSONObject(responseValue);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        emergencyServiceModels = new ArrayList<>();
                        for(int i = 0;i < jsonArray.length();i++){
                            JSONObject elem = jsonArray.getJSONObject(i);

                            id=elem.getString("id");
                            title = elem.getString("contact_person_or_institute");
                            locations = elem.getString("location");
                            number = elem.getString("phone_number");
                            //connectToAdapter(title,body);

                            emergencyServiceModels.add(new EmergencyModel(id,locations, number,title));

                        }

                        EmergencyServiceAdapter mAdapter = new EmergencyServiceAdapter(emergencyServiceModels,phoneListener,getContext());
                        recyclerView.setAdapter(mAdapter);
                        Log.d("datajsonarray","this is the value of json "+notification );
                        progress.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }else{
                    Toast.makeText(getContext(),"response fail",Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getContext(), "No internet conenction", Toast.LENGTH_LONG).show();
            }
        });

    }




    public void onAdapterListeners(){
        phoneListener = new PhoneListener() {

            @Override
            public void onItemClicked(String phonenumber) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
//                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                    callIntent.setData(Uri.parse("tel:"+phonenumber));
//                    startActivity(callIntent);
                }
                else
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+phonenumber));
                    startActivity(callIntent);
                }

            }
        };
    }

}

