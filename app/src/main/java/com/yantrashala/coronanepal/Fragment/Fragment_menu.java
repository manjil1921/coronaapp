package com.yantrashala.coronanepal.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.yantrashala.coronanepal.ChildAnimationExample;
import com.yantrashala.coronanepal.R;
import com.yantrashala.coronanepal.Route;
import com.yantrashala.coronanepal.SliderLayout;

import java.util.HashMap;

public class Fragment_menu extends Fragment implements View.OnClickListener {

    View view;
    SliderLayout mDemoSlider;
    CardView cardViewDo,cardViewNot, cardViewSymptoms,cardViewTreatment,cardViewAbout;
    ImageView imgAbout,imgDo,imgNot,imgSymptoms,imgTreatment;
    TextView txtAbout,txtDo,txtNot,txtSymptoms,txtTreatment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sliding();

        intii();

    }


    public void sliding() {

        mDemoSlider = getView().findViewById(R.id.slider);

        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1", R.drawable.crowd);
        file_maps.put("2", R.drawable.mask);
        file_maps.put("3", R.drawable.corona);


        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    //  .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            //    .setOnSliderClickListener(getActivity());


            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new ChildAnimationExample());
        mDemoSlider.setDuration(3000);
        mDemoSlider.addOnPageChangeListener(getActivity());

    }


    public void onSliderClick(BaseSliderView slider) {

    }


//    public void click(){
//
//        imgAbout=(ImageView) getView().findViewById(R.id.img_about);
//        imgAbout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_About llf = new Fragment_About();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        txtAbout=(TextView) getView().findViewById(R.id.txt_about);
//        imgAbout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_About llf = new Fragment_About();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//
//        cardViewAbout=(CardView)getView().findViewById(R.id.card_about);
//        cardViewAbout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_About llf = new Fragment_About();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//
//
//        imgDo=(ImageView) getView().findViewById(R.id.img_Do);
//        imgDo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Whattodo llf = new Fragment_Whattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//        txtDo=(TextView) getView().findViewById(R.id.txt_Do);
//        txtDo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Whattodo llf = new Fragment_Whattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//        cardViewDo=(CardView)getView().findViewById(R.id.card_Do);
//        cardViewDo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Whattodo llf = new Fragment_Whattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        imgNot=(ImageView) getView().findViewById(R.id.img_NotDo);
//        imgNot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_NotWhattodo llf = new Fragment_NotWhattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        txtNot=(TextView) getView().findViewById(R.id.txt_NotDo);
//        txtNot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_NotWhattodo llf = new Fragment_NotWhattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        cardViewNot=(CardView)getView().findViewById(R.id.card_NotDo);
//        cardViewNot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_NotWhattodo llf = new Fragment_NotWhattodo();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//
//
//        imgSymptoms=(ImageView) getView().findViewById(R.id.img_symptoms);
//        imgSymptoms.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Symptoms llf = new Fragment_Symptoms();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//
//        txtSymptoms=(TextView) getView().findViewById(R.id.txt_symptoms);
//        txtSymptoms.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Symptoms llf = new Fragment_Symptoms();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        cardViewSymptoms=(CardView)getView().findViewById(R.id.card_symptoms);
//        cardViewSymptoms.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Symptoms llf = new Fragment_Symptoms();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        imgTreatment=(ImageView) getView().findViewById(R.id.imgTreatment);
//        imgTreatment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Treatment llf = new Fragment_Treatment();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        txtTreatment=(TextView) getView().findViewById(R.id.txtTreatment);
//        txtTreatment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Treatment llf = new Fragment_Treatment();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//
//        cardViewTreatment=(CardView)getView().findViewById(R.id.card_treatment);
//        cardViewTreatment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                Fragment_Treatment llf = new Fragment_Treatment();
//                ft.replace(R.id.main_fragment, llf);
//                ft.commit();
//            }
//        });
//    }


    public void intii(){
        cardViewAbout = getView().findViewById(R.id.card_about);
        imgAbout=getView().findViewById(R.id.img_about);
        txtAbout=getView().findViewById(R.id.txt_about);

        cardViewDo = getView().findViewById(R.id.card_Do);
        txtDo = getView().findViewById(R.id.txt_Do);
        imgDo = getView().findViewById(R.id.img_Do);

        cardViewNot = getView().findViewById(R.id.card_NotDo);
        txtNot = getView().findViewById(R.id.txt_NotDo);
        imgNot = getView().findViewById(R.id.img_NotDo);

        cardViewSymptoms = getView().findViewById(R.id.card_symptoms);
        txtSymptoms = getView().findViewById(R.id.txt_symptoms);
        imgSymptoms = getView().findViewById(R.id.img_symptoms);

        cardViewTreatment = getView().findViewById(R.id.card_treatment);
        txtTreatment = getView().findViewById(R.id.txtTreatment);
        imgTreatment = getView().findViewById(R.id.imgTreatment);

        setOnClick();
    }

    public void setOnClick(){
        cardViewAbout.setOnClickListener(this);
        txtAbout.setOnClickListener(this);
        imgAbout.setOnClickListener(this);

        cardViewDo.setOnClickListener(this);
        txtDo.setOnClickListener(this);
        imgDo.setOnClickListener(this);

        cardViewNot.setOnClickListener(this);
        txtNot.setOnClickListener(this);
        imgNot.setOnClickListener(this);

        cardViewSymptoms.setOnClickListener(this);
        txtSymptoms.setOnClickListener(this);
        imgSymptoms.setOnClickListener(this);

        cardViewTreatment.setOnClickListener(this);
        txtTreatment.setOnClickListener(this);
        imgTreatment.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.card_about){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_About(), "a", true);

        }
        if(id == R.id.img_about){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_About(), "a", true);

        }
        if(id == R.id.txt_about){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_About(), "a", true);

        }


        if(id == R.id.card_Do){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Whattodo(), "a", true);

        }
        if(id == R.id.img_Do){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Whattodo(), "a", true);

        }
        if(id == R.id.txt_Do){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Whattodo(), "a", true);

        }


        if(id == R.id.card_NotDo){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_NotWhattodo(), "a", true);

        }
        if(id == R.id.txt_NotDo){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_NotWhattodo(), "a", true);

        }
        if(id == R.id.img_NotDo){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_NotWhattodo(), "a", true);

        }


        if(id == R.id.card_symptoms){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Symptoms(), "a", true);

        }
        if(id == R.id.txt_symptoms){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Symptoms(), "a", true);

        }
        if(id == R.id.img_symptoms){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Symptoms(), "a", true);

        }


        if(id == R.id.card_treatment){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Treatment(), "a", true);

        }
        if(id == R.id.txtTreatment){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Treatment(), "a", true);

        }
        if(id == R.id.imgTreatment){
            Route.attachFragment((AppCompatActivity) getContext(), R.id.main_fragment, new Fragment_Treatment(), "a", true);

        }
    }
}
