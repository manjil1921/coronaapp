package com.yantrashala.coronanepal.Fragment;

import android.content.Intent;
import android.os.Bundle;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.yantrashala.coronanepal.ImageView_zoom;
import com.yantrashala.coronanepal.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;

public class Fragment_Home_Main_Description extends Fragment {
    String imageh,des,shorth_desh,tith,logoimg;
    private View view;
    ImageView main_image,logo_image;
    TextView titles,descriptions,long_description;
//    String title,image,description,shortdescription;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_home_page,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageh = getArguments().getString("image");
        des = getArguments().getString("long_description");
        shorth_desh = getArguments().getString("short_description");
        tith = getArguments().getString("title");
        init();
        zoomimage();
    }

    public void init(){
        main_image = getView().findViewById(R.id.home_main_image);
        titles = getView().findViewById(R.id.home_main_image_title);
        descriptions = getView().findViewById(R.id.home_main_image_des);
        long_description = getView().findViewById(R.id.home_main_image_des1);

        try {
            URL uri = new URL(imageh);
            Picasso.with(getContext())

                    .load(String.valueOf(uri))

                    .into(main_image);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

//        try {
//            URL uri = new URL(imageh);
//            Picasso.with(getContext())
//                    .load(String.valueOf(uri))
//                    .into(main_image);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

        titles.setText(Html.fromHtml(tith));
        descriptions.setText(Html.fromHtml(shorth_desh));
        long_description.setText(Html.fromHtml(des));

    }

    public void zoomimage(){

        main_image = getView().findViewById(R.id.home_main_image);
        main_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mintent = new Intent(getContext(), ImageView_zoom.class);
                mintent.putExtra("image",imageh);
                startActivity(mintent);
            }
        });

    }
}
