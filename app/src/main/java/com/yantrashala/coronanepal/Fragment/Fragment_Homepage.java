package com.yantrashala.coronanepal.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.yantrashala.coronanepal.API.RetrofitHelper;
import com.yantrashala.coronanepal.API.CoranaAPI;
import com.yantrashala.coronanepal.Adapter.HomePageAdapter;
import com.yantrashala.coronanepal.Interface.HomePageValueListener;
import com.yantrashala.coronanepal.Model.HomeModels;
import com.yantrashala.coronanepal.R;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Homepage extends Fragment {
    Toolbar toolbar;
    Activity mActivity;
    RecyclerView recyclerView;
    ArrayList<HomeModels> homeModels;
    HomePageAdapter homePageAdapter;
    String id, image, home_des, date_added, long_des, title,source;
    private HomePageValueListener homePageValueListener;
    ProgressDialog dialog;
    CardView minterface;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_homepage, container, false);


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
        dialog = new ProgressDialog(getContext());
        initToolbar();
        findview();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initToolbar() {
        AppCompatActivity mAppCompatActivity = (AppCompatActivity) mActivity;
        toolbar = mAppCompatActivity.findViewById(R.id.toolbar);
        toolbar.setTitle("\n" +
                "समाचार र अपडेटहरू");
        toolbar.setTitleTextColor(this.getResources().getColor(R.color.colorWhite));
        mAppCompatActivity.setSupportActionBar(toolbar);
        ActionBar actionBar = mAppCompatActivity.getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setLogo(R.drawable.corona);
//            actionBar.setDisplayHomeAsUpEnabled(false);
//        }

    }

    public void findview() {

        recyclerView = getView().findViewById(R.id.recycler_homepage);
        init();
    }

    public void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        getdata();
        onAdapterListeners();
    }

//    public boolean isconnectd() {
//        ConnectivityManager mconn = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo minfo = mconn.getActiveNetworkInfo();
//        if (minfo != null && minfo.isConnected()) {
//            return true;
//        }
//        return false;
//    }

    private void getdata() {
        Log.d("hey","this is called");
        dialog.setMessage("Loading data...");
        dialog.show();
        CoranaAPI service = (CoranaAPI) RetrofitHelper.getInstance().getService(CoranaAPI.class);
        Call<JsonElement> call = service.getNews();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                Log.d("hey","afkafal"+response);
                if (response.isSuccessful()) {
                    Log.d("affafa","this is called");
                    String responseValue = response.body().toString();
                    JSONArray notification = null;
                    try {
                        JSONObject jsonObject = new JSONObject(responseValue);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        homeModels = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject elem = jsonArray.getJSONObject(i);

                            id = elem.getString("id");
                            title = elem.getString("title");
                            long_des = elem.getString("long_description");
                            home_des = elem.getString("short_description");
                            image = elem.getString("image");
                            date_added = elem.getString("date_added");
                            source=elem.getString("source");


                            homeModels.add(new HomeModels(id, title, source,  date_added,  long_des,home_des,image));

                        }

                        homePageAdapter = new HomePageAdapter(homeModels,homePageValueListener ,getContext());
                        recyclerView.setAdapter(homePageAdapter);
                        Log.d("datajsonarray", "this is the value of json " + notification);
                        dialog.dismiss();

                    }

                    catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(getContext(), "No internet conenction", Toast.LENGTH_LONG).show();




                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("hey",""+t);
                dialog.dismiss();
                Toast.makeText(getContext(), ""+t, Toast.LENGTH_LONG).show();



            }
        });

    }




    public void onAdapterListeners() {
        homePageValueListener = new HomePageValueListener() {

            @Override
            public void onItemClick(String title, String source, String image, String date_added,String long_description,String short_description) {
                Log.d("short_desths",short_description);
                Fragment_Home_Main_Description fragment_home_main_description = new Fragment_Home_Main_Description();
                Bundle bd = new Bundle();
                bd.putString("image", image);
                bd.putString("long_description", long_description);
                bd.putString("short_description", short_description);
                bd.putString("title", title);
                bd.putString("image",image);
                fragment_home_main_description.setArguments(bd);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager != null ? fragmentManager.beginTransaction() : null;
                assert fragmentTransaction != null;
                fragmentTransaction.replace(R.id.main_fragment, fragment_home_main_description).addToBackStack("back").commit();
            }
        };
    }

}
