package com.yantrashala.coronanepal.Interface;

public interface HomePageValueListener {

    void onItemClick(String title, String source, String image, String date_added, String long_description, String short_description);
}
