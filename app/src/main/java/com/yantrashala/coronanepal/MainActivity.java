package com.yantrashala.coronanepal;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yantrashala.coronanepal.Fragment.FragmentAvailable;
import com.yantrashala.coronanepal.Fragment.Fragment_Emergency;
import com.yantrashala.coronanepal.Fragment.Fragment_Homepage;
import com.yantrashala.coronanepal.Fragment.Fragment_menu;

public class MainActivity extends AppCompatActivity {
    Context mContext;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {


                case R.id.nav_available:
                    Route.attachFragment(MainActivity.this, R.id.main_fragment, new FragmentAvailable(), "a", false);
                    return true;
                case R.id.nav_menu:
                    Route.attachFragment(MainActivity.this, R.id.main_fragment, new Fragment_menu(), "a", false);
                    return true;
                case R.id.nav_news:
                    Route.attachFragment(MainActivity.this, R.id.main_fragment, new Fragment_Homepage(), "a", false);
                    return true;

                case R.id.nav_emergency:
                    Route.attachFragment(MainActivity.this, R.id.main_fragment, new Fragment_Emergency(), "a", false);
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        BottomNavigationViewHelper.disableShiftMode(navigation);

        mContext = this;

        if( savedInstanceState == null )
            changeFragment(new Fragment_Homepage());
    }


    private void changeFragment(Fragment targetFragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }





    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.corona)
                    .setTitle("Close Application")
                    .setMessage("Do you want to Exit?")
                    .setNeutralButton("Rate Us", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                                startActivity(marketIntent);
                            }catch(ActivityNotFoundException e) {
                                Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                                startActivity(marketIntent);
                            }

                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        } else {
            MainActivity.super.onBackPressed();
        }
    }

}



