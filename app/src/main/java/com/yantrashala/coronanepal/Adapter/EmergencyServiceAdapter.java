package com.yantrashala.coronanepal.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.yantrashala.coronanepal.Interface.PhoneListener;
import com.yantrashala.coronanepal.Model.EmergencyModel;
import com.yantrashala.coronanepal.R;

import java.util.ArrayList;
import java.util.List;

public class EmergencyServiceAdapter extends RecyclerView.Adapter<EmergencyServiceAdapter.MyViewHolder> {

    private List<EmergencyModel> emergencyServiceModels;
    private Context context;
    private PhoneListener phoneListener;


    public  EmergencyServiceAdapter(ArrayList<EmergencyModel> emergencyServiceModels, PhoneListener phoneListener, Context context) {
        this.emergencyServiceModels = emergencyServiceModels;
        this.context = context;
        this.phoneListener = phoneListener;
    }


    @Override
    public  MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemVIew = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_emergency, parent, false);
        return new MyViewHolder(itemVIew, context);
    }


    @Override
    public void onBindViewHolder(EmergencyServiceAdapter.MyViewHolder holder, int position) {
        final EmergencyModel emergencyServiceModel = emergencyServiceModels.get(position);

        holder.locations.setText(emergencyServiceModel.getLocation());
        holder.titles.setText(emergencyServiceModel.getContact_person_or_institute());
        holder.numbers.setText(emergencyServiceModel.getPhone_number());
        holder.phonenum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phonenum = emergencyServiceModel.getPhone_number();
                sendNum(phonenum);
            }
        });

    }

    @Override
    public int getItemCount() {
        return emergencyServiceModels.size();
    }


    public void sendNum(String phoneNum){
        phoneListener.onItemClicked(phoneNum);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titles,numbers,locations;
        ImageView phonenum;

        public MyViewHolder(View itemView, final Context context) {
            super(itemView);
            titles = itemView.findViewById(R.id.bci_title);
            numbers = itemView.findViewById(R.id.bci_phone);
            locations = itemView.findViewById(R.id.bci_location);
            phonenum = itemView.findViewById(R.id.imgphone);


        }
    }
}