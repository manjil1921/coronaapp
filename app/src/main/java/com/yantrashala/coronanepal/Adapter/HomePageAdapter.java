package com.yantrashala.coronanepal.Adapter;


import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yantrashala.coronanepal.Interface.HomePageValueListener;
import com.yantrashala.coronanepal.Model.HomeModels;
import com.yantrashala.coronanepal.R;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class HomePageAdapter extends RecyclerView.Adapter<HomePageAdapter.MyViewHolder> {

    private List<HomeModels> homeModels;
    private Context context;
    private HomePageValueListener homePageValueListener;




    public HomePageAdapter(ArrayList<HomeModels> homeModels, HomePageValueListener homePageValueListener, Context context) {
        this.homeModels = homeModels;
        this.context = context;
        this.homePageValueListener = homePageValueListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemVIew = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_homepage, parent, false);
        return new MyViewHolder(itemVIew, context);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final HomeModels hmodel = homeModels.get(position);
        final String des = hmodel.getLong_description();
        final String short_des = hmodel.getShort_description();
        final String date_added = hmodel.getDate_added();
        final String tit = hmodel.getTitle();
        final String source = hmodel.getSource();
        holder.date.setText(hmodel.getDate_added());
        Log.d("hey","The image is "+tit);
        final String image = hmodel.getImage();
        try {
            URL uri = new URL(image);
            Picasso.with(context)
                    .load(String.valueOf(uri))
                    .into(holder.image);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = hmodel.getId();
                getId(image,tit,source,des,short_des,date_added);
            }

        });

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = hmodel.getId();
                getId(image,tit,source,des,short_des,date_added);
            }

        });

        holder.short_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = hmodel.getId();
                getId(image,tit,source,des,short_des,date_added);
            }

        });

        holder.title.setText(Html.fromHtml(hmodel.getTitle()));
        holder.short_description.setText(Html.fromHtml(hmodel.getShort_description()));
    }

    public void getId(String image, String title, String source, String long_description, String short_description, String date_added){
        homePageValueListener.onItemClick(title,source,image,date_added,long_description,short_description);    }


    @Override
    public int getItemCount() {
        return homeModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView date, title,description, short_description;
        Button btn_download;

        public MyViewHolder(View itemView, final Context context) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
            date = itemView.findViewById(R.id.txt_home_date);
            image = itemView.findViewById(R.id.image_homepageImage);
//            description = itemView.findViewById(R.id.long_home_des);
            short_description = itemView.findViewById(R.id.short_des);
//            btn_download = itemView.findViewById(R.id.btn_homepage);

        }
    }
}

