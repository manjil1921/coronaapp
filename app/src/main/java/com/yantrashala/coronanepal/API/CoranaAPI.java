package com.yantrashala.coronanepal.API;
import com.google.gson.JsonElement;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CoranaAPI {

    @GET("news/")
    Call<JsonElement> getNews();


    @GET("aapatkalin/")
    Call<JsonElement> getImpNumber();

}